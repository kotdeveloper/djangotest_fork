from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from djangotest.api import client_api
from djangotest.api import UISettingsResource
from tastypie.api import Api

v1_api = Api(api_name='v1')
v1_api.register(UISettingsResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djangotest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^api/', include(client_api.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'', include(admin.site.urls)),
)

